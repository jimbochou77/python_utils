import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

# 31 wavelengths
wavelengths = np.arange(400, 710, 10)
intensities = [10] * 15
intensities.extend([20] * 10)
intensities.extend([15] * 6)

data_x = wavelengths
data_y = intensities

# Automatically make room for elements in the figure.
plt.rcParams.update({'figure.autolayout': True})

# Generate the instances of Figure and Axes.
# Also change size of the figure.
fig, ax = plt.subplots(figsize=(8,4))

# Plot on top of the Axes instance
ax.plot(data_x, data_y)

# Change the style
plt.style.use('fivethirtyeight')

# Get the current labels and attempt to set some style element to each one.
labels = ax.get_xticklabels()
plt.setp(labels, rotation=45, horizontalalignment='right')

# Set the range
# Even though we set the range from 0 to 750, the actual data_y limits what is plotted.
plt.xticks(np.arange(0, 750, step=20))

# Add labels to the plot.
ax.set(xlim=[400, 700], xlabel='Wavelength', ylabel='Intensity', title='SPD')


# Add a vertical line, here we set the style in the function call
ax.axvline(685, ls='--', color='r')
ax.axvline(530, ls='--', color='g')
ax.axvline(470, ls='--', color='b')

# Now we'll move our title up since it's getting a little cramped
ax.title.set(y=1.05)


plt.show()