# sphinx_gallery_thumbnail_number = 10
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter


data = {'Barton LLC': 109438.50,
        'Frami, Hills and Schmidt': 103569.59,
        'Fritsch, Russel and Anderson': 112214.71,
        'Jerde-Hilpert': 112591.43,
        'Keeling LLC': 100934.30,
        'Koepp Ltd': 103660.54,
        'Kulas Inc': 137351.96,
        'Trantow-Barrows': 123381.38,
        'White-Trantow': 135841.99,
        'Will LLC': 104437.60}
group_data = list(data.values())
group_names = list(data.keys())
group_mean = np.mean(group_data)


# Take an integer input and output a string.
def currency(x, pos):
    """The two args are the value and tick position"""
    if x >= 1e6:
        s = '${:1.1f}M'.format(x*1e-6)
    else:
        s = '${:1.0f}K'.format(x*1e-3)
    return s

formatter = FuncFormatter(currency)

# Automatically make room for elements in the figure.
plt.rcParams.update({'figure.autolayout': True})

# Generate the instances of Figure and Axes.
# Also change size of the figure.
fig, ax = plt.subplots(figsize=(8,4))

# Plot on top of the Axes instance
ax.barh(group_names, group_data)

# Change the style
plt.style.use('fivethirtyeight')

# Get the current labels and attempt to set some style element to each one.
labels = ax.get_xticklabels()
plt.setp(labels, rotation=45, horizontalalignment='right')

# Add labels to the plot.
ax.set(xlim=[-10000, 140000], xlabel='Total Revenue', ylabel='Company', title='Company Revenue')

# Apply the formatter to the labels.
ax.xaxis.set_major_formatter(formatter)

# Add a vertical line, here we set the style in the function call
ax.axvline(group_mean, ls='--', color='r')

# Annotate new companies
for group in [3, 5, 8]:
    ax.text(145000, group, "New Company", fontsize=10, verticalalignment="center")

# Now we'll move our title up since it's getting a little cramped
ax.title.set(y=1.05)

# Uncomment this line to save the figure.
fig.savefig('sales.png', transparent=False, dpi=80, bbox_inches="tight")

plt.show()